using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Test.Umbraco.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class FileTypeAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly IEnumerable<string> _validTypes;

        public FileTypeAttribute(string fileExtensions)
        {
            _validTypes = fileExtensions.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;

            if (file != null)
            {
                var fileName = file.FileName;
                return _validTypes.Any(type => fileName.EndsWith(type));
            }

            return true;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ValidationType = "filetype",
                ErrorMessage = FormatErrorMessage(metadata.GetDisplayName())
            };
            rule.ValidationParameters["validtypes"] = string.Join(",", _validTypes);
            yield return rule;
        }
    }
}