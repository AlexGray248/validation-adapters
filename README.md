#Usage
Load js after standard unobtrusive js.

Add data annotation to property of type eg
```
[Required]
[Attributes.FileType("jpeg,jpg,gif,bmp,png,tiff,tif", ErrorMessage = "image file types")]
public HttpPostedFileBase TrackArtwork { get; set; }
```

Can change to load error message from dictionary.

Also would be possible to load the types from web.config to be able to change at runtime