$(function () {
    function getFileExtension(fileName) {
        if (/[.]/.exec(fileName)) {
            return /[^.]+$/.exec(fileName)[0].toLowerCase();
        }
        return null;
    }

    jQuery.validator.unobtrusive.adapters.add("filetype", ["validtypes"], function (options) {
        options.rules["filetype"] = {
            validtypes: options.params.validtypes.split(",")
        };
        options.messages["filetype"] = options.message;
    });

    jQuery.validator.addMethod("filetype", function (value, element, param) {
        var extension = getFileExtension(element.files[0].name);
        if ($.inArray(extension, param.validtypes) === -1) {
            return false;
        }
        return true;
    });
}(jQuery));